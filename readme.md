Conda recipes for skmad-suite
=============================

This repository provides conda recipes for the skmad-suite packages and some of
their dependencies.

The recipes are available in the directory [recipes](recipes) and can be
used to build conda packages for Linux and macOS. The resulting packages
are released through the [skmad channel](https://anaconda.org/skmad) on
anaconda.org.

The directory [docker](docker) contains a Dockerfile that can be used to create
a Linux Docker image suitable for the generation of the conda packages for
Linux.

The resulting Docker image is available via the registry of this GitLab
project. You can run it with:
```bash
docker run -it registry.gitlab.lis-lab.fr:5005/skmad-suite/conda_recipes/ubuntu:16.04
```

Building the Docker image for Linux
-----------------------------------

If needed, you can build the Docker image with:
```bash
docker build -t conda_builder docker
```
You can then run this image in interactive mode to build the conda packages
with:
```bash
docker run -it conda_builder
```

Building and uploading the conda packages
-----------------------------------------

To build the conda packages, you need to install the conda package manager,
the simplest solution for this being to install
[miniconda](https://conda.io/miniconda.html).

In your conda environment, if not already installed, install the `conda-build`
and `anaconda-client` packages:
```bash
conda install conda-build anaconda-client
```

Clone the conda_recipes repository:
```bash
git clone https://gitlab.lis-lab.fr/skmad-suite/conda_recipes.git
```

Use conda to build the package of your choice. See the readme.md file in
the diretory containing the recipe that you want to build for detailed
build and upload instructions.
