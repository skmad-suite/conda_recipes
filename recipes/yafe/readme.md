Building and uploading the yafe conda package
---------------------------------------------

To build the package, from the current directory, run:
```bash
conda build .
```

Once built, a package can be uploaded to anaconda.org using the Anaconda
client:
```bash
anaconda upload --user skmad /miniconda/conda-bld/noarch/yafe*
```

Note: in the previous command the path of the uploaded file should be adapted
according to the installation directory of your conda distribution.
