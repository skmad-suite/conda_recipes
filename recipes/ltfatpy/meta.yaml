{% set name = "ltfatpy" %}
{% set version = "1.0.16" %}
{% set file_ext = "tar.gz" %}
{% set hash_type = "sha256" %}
{% set hash_value = "187e2fab9fa513be5af96b2d38f2d3cd4bad05981a762a155b583b4cefeec53b" %}

package:
  name: '{{ name|lower }}'
  version: '{{ version }}'

source:
  fn: '{{ name }}-{{ version }}.{{ file_ext }}'
  url: https://pypi.io/packages/source/{{ name[0] }}/{{ name }}/{{ name }}-{{ version }}.{{ file_ext }}
  '{{ hash_type }}': '{{ hash_value }}'

build:
  number: 0
  script: python setup.py install --single-version-externally-managed --record=record.txt

requirements:
  host:
    - nomkl
    - cmake
    - make
    - fftw
    - openblas
    - lapack
    - cython
    - python {{ python_version }}
    - setuptools
    - scipy >=0.18
    - numpy >=1.8
    - matplotlib >=1.4
    - six >=1.10
  run:
    - nomkl
    - fftw
    - openblas
    - python
    - scipy >=0.18
    - numpy >=1.8
    - matplotlib >=1.4
    - six >=1.10

test:
  requires:
    - pytest
  commands:
    - pytest --pyargs ltfatpy

about:
  home: https://gitlab.lis-lab.fr/dev/ltfatpy
  license: GPL-3.0
  license_family: GPL3
  license_file: LICENSE.rst
  summary: The Large Time-Frequency Toolbox (LTFAT) in Python
  description: |
    The **ltfatpy** package is a partial Python port of the
    [Large Time/Frequency Analysis Toolbox (LTFAT)](http://ltfat.sourceforge.net),
    a MATLAB©/Octave toolbox for working with time-frequency analysis and
    synthesis.
    
    It is intended both as an educational and a computational tool.
    
    The package provides a large number of linear transforms including Gabor
    transforms along with routines for constructing windows (filter
    prototypes) and routines for manipulating coefficients.
    
    The original LTFAT Toolbox for MATLAB©/Octave is developed at
    [CAHR](http://www.dtu.dk/centre/cahr/English.aspx), Technical University
    of Denmark, [ARI](http://www.kfs.oeaw.ac.at), Austrian Academy of
    Sciences and [I2M](http://www.i2m.univ-amu.fr), Aix-Marseille
    Université.
    
    The Python port is developed at
    [LabEx Archimède](http://labex-archimede.univ-amu.fr), as a
    [LIF](http://www.lif.univ-mrs.fr/) (now [LIS](http://www.lis-lab.fr/)) and
    I2M project, Aix-Marseille Université.
    
    This package, as well as the original LTFAT toolbox, is Free software,
    released under the GNU General Public License (GPLv3).
  doc_url: http://dev.pages.lis-lab.fr/ltfatpy
  dev_url: https://gitlab.lis-lab.fr/dev/ltfatpy

extra:
  recipe-maintainers: Florent Jaillet <contact.dev@lis-lab.fr>

