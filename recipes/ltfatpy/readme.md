Building and uploading the ltfatpy conda package
------------------------------------------------

To build the package, from the current directory, run:
```bash
conda build -c anaconda -c conda-forge .
```

Note: In the previous command, we specify the channel `conda-forge`
because the `ltfatpy` package requires packages that are not available in
Anaconda but are provided by the channel `conda-forge`.
We also specify `anaconda` as the first channel, because we want to
preferentially use packages from Anaconda for packages that are both available
in Anaconda and conda-forge.

Once built, a package can be uploaded to anaconda.org using the Anaconda
client:
```bash
anaconda upload --user ltfatpy /miniconda/conda-bld/linux-64/ltfatpy*
```

Note: in the previous command the path of the uploaded file should be adapted
according to the installation directory of your conda distribution and the
architecture for which the package has been built.
